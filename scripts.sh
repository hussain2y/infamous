#!/usr/bin/env bash

composer update
composer dump-autoload

php artisan key:generate
php artisan config:clear
php artisan config:cache
