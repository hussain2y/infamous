<h3>Installation Process</h3>
<p>
    <span class="text-center">Please Follow the following steps to install and run the Assignment successfully.</h5>
    <ul>
        <li>
            <strong>Step 1: Clone Repository</strong> git clone git@gitlab.com:hussain2y/infamous.git (In case added ssh key) OR git clone https://gitlab.com/hussain2y/infamous.git. 
        </li>
        <li>
            <strong>Step 2: Update Composer</strong> After cloning the repo, go to the dir where you have cloned and run the `composer install` command. 
        </li>
        <li>
            <strong>Step 3: Set Environment</strong> Run the command `cp .env.example .env` and set your database connection inside .env file. 
        </li>
        <li>
            <strong>Step 4: Run Migrations</strong> Run the command `php artisan migrate`. For Linux Users please provide access to bootstrap and storage dir if migration are not running. 
        </li>
        <li>
            <strong>Step 5: Install Node Modules</strong> Run the command `npm install`, to install node packages used in this assignment.  
        </li>
        <li>
            <strong>Step 5: Run Application</strong> After all the process runs cuccesfully, now its time to run the application. To check Vue.js components work properly, run the cmd: npm run watch-poll, and to check it on localhost, run the command: php artisan serve.  
        </li>
    </ul>
    <span>Thanks for your patience.</span>
</p>

