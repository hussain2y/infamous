@extends('layouts.app')

@section('title') {{ trans('auth.create_user') }} @stop

@section('content') <create-user></create-user> @endsection
