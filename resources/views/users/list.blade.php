@extends('layouts.app')

@section('title') {{ trans('auth.users_list') }} @stop

@section('content') <users-list></users-list> @stop