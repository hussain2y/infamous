<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return User::orderBy('name', 'ASC')->get();
        } else {
            return view('users.list');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {

        $date = Carbon::now()->subYears(18)->format('Y-m-d');

        $validator = Validator::make($request->all(), [
            'name'      => 'required|min:5|max:30',
            'email'     => 'required|email|max:50',
            'phone'     => 'required|min:11|max:15',
            'gender'    => 'required|in:male,female',
            'dob'       => 'required|date|before_or_equal:'.$date,
            'biography' => 'required|min:10|max:100',
            'avatar'    => 'required|mimes:jpeg,png|max:1024'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::create([
            'name'      => request('name'),
            'email'     => request('email'),
            'phone'     => request('phone'),
            'gender'    => request('gender'),
            'dob'       => Carbon::parse(request('dob')),
            'biography' => request('biography'),
        ]);

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $original_file = time().uniqid().$file->getClientOriginalName();
            $file_path = base_path('public/avatars/');
            $relative_path = '/avatars/'.$original_file;

            if ($file->isValid()) {
                $file->move($file_path, $original_file);
                $user->avatar = $relative_path;
                $user->save();
            }
        }

        $request->session()->flash('success', trans('auth.user_created_successfully'));

        return redirect('users');
    }
}
