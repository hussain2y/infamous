<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 * @property mixed name
 * @property mixed email
 * @property mixed phone
 * @property mixed dob
 * @property mixed gender
 * @property mixed biography
 * @property mixed avatar
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'dob', 'gender', 'biography', 'avatar'
    ];

    /**
     * The attributes that must be Carbon Objects (Timestamp)
     *
     * @var array
     */
    protected $dates = ['dob'];
}
